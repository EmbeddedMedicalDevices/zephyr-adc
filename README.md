# Zephyr: Analog-to-Digital Conversion (ADC)

[![pipeline status](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-adc/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-adc/-/commits/main)

* HTML of Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/zephyr-adc/Zephyr-ADC.html
* PDF of Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/zephyr-adc/Zephyr-ADC.pdf